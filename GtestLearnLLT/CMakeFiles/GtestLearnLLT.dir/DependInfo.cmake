# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/yjie/GtestLearn/func.c" "/home/yjie/GtestLearnLLT/CMakeFiles/GtestLearnLLT.dir/home/yjie/GtestLearn/func.c.o"
  "/home/yjie/GtestLearnLLT/stubs/my_stubs.c" "/home/yjie/GtestLearnLLT/CMakeFiles/GtestLearnLLT.dir/stubs/my_stubs.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "third_party/googletest/include"
  "third_party/googlemock/include"
  "third_party/mockcpp/include"
  "third_party/mockcpp/3rdparty"
  "third_party/googlemock"
  "../GtestLearn/include"
  "stubs"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yjie/GtestLearnLLT/gtest_ut.cpp" "/home/yjie/GtestLearnLLT/CMakeFiles/GtestLearnLLT.dir/gtest_ut.cpp.o"
  "/home/yjie/GtestLearnLLT/main.cpp" "/home/yjie/GtestLearnLLT/CMakeFiles/GtestLearnLLT.dir/main.cpp.o"
  "/home/yjie/GtestLearnLLT/third_party/googlemock/src/gmock-all.cc" "/home/yjie/GtestLearnLLT/CMakeFiles/GtestLearnLLT.dir/third_party/googlemock/src/gmock-all.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "third_party/googletest/include"
  "third_party/googlemock/include"
  "third_party/mockcpp/include"
  "third_party/mockcpp/3rdparty"
  "third_party/googlemock"
  "../GtestLearn/include"
  "stubs"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
